## Converting a number to words


This is a converting function.

It converts a number to an English word with a specific condition of adding "," after thousands, "and" after hundreds, and "-" after tens.

And it has a function to ensure that the input integer is between 0-10000.

If the input integer is between 0-10000, it will be automatically converted to an English word through the above functionality.

However, if it is not between 0-10000, "Invalid number! Please input a number between 0-10000" message will be shown.
 

## Codes

⇩ a function to ensure that the input integer is between 0-10000
```
>def raw_input(input_return):
>    try:
>        input_int=int(input_return)
>        if not 0<input_int<10000:
>            print ("Invalid number! please input a number between 0-10000")
>            return None
>        return input_int
>    except ValueError:
>            print ("The input was not a valid integer.")
>            return None
>while True:
>    input_int=raw_input(int(input("Input a number between 0-10000 :  ")))
>    if input_int:break
```

⇩ converts a number to an English word.
```
def convert(input_int):
    one_to_nineteen = ("","one ","two ","three ","four ","five ","six ","seven ","eight ","nine ","ten ","eleven ",
                        "twelve ","thirteen ","fourteen ","fifteen ","sixteen ","seventeen ","eighteen ","nineteen ")
    tens =("","","twenty","thirty","forty","fifty","sixty","seventy","eighty","ninety")
```

⇩ converts a number to an English word with a specific condition of adding "-" between tens and the last digit.
```
    if input_int<20:
        return  one_to_nineteen[input_int] 
    if input_int==20:
        return  tens[input_int // 10] +one_to_nineteen[int(input_int % 10)] 
    if input_int==30:
        return  tens[input_int // 10] +one_to_nineteen[int(input_int % 10)] 
    if input_int==40:
        return  tens[input_int // 10] +one_to_nineteen[int(input_int % 10)] 
    if input_int==50:
        return  tens[input_int // 10] +one_to_nineteen[int(input_int % 10)] 
    if input_int==60:
        return  tens[input_int // 10] +one_to_nineteen[int(input_int % 10)] 
    if input_int==70:
        return  tens[input_int // 10] +one_to_nineteen[int(input_int % 10)] 
    if input_int==80:
        return  tens[input_int // 10] +one_to_nineteen[int(input_int % 10)] 
    if input_int==90:
        return  tens[input_int // 10] +one_to_nineteen[int(input_int % 10)] 
    elif input_int<100:
        return  tens[input_int // 10] + "-" +one_to_nineteen[int(input_int % 10)] 
```

⇩ converts a number to an English word with a specific condition of adding "and" after hundreds.
```
    if input_int==100:
        return one_to_nineteen[input_int // 100]  + "hundred" +convert(int(input_int % 100))
    if input_int==200:
        return one_to_nineteen[input_int // 100]  + "hundred" +convert(int(input_int % 100))
    if input_int==300:
        return one_to_nineteen[input_int // 100]  + "hundred" +convert(int(input_int % 100))
    if input_int==400:
        return one_to_nineteen[input_int // 100]  + "hundred" +convert(int(input_int % 100))
    if input_int==500:
        return one_to_nineteen[input_int // 100]  + "hundred" +convert(int(input_int % 100))
    if input_int==600:
        return one_to_nineteen[input_int // 100]  + "hundred" +convert(int(input_int % 100))
    if input_int==700:
        return one_to_nineteen[input_int // 100]  + "hundred" +convert(int(input_int % 100))
    if input_int==800:
        return one_to_nineteen[input_int // 100]  + "hundred" +convert(int(input_int % 100))
    if input_int==900:
        return one_to_nineteen[input_int // 100]  + "hundred" +convert(int(input_int % 100))
    elif input_int<1000:
        return one_to_nineteen[input_int // 100]  + "hundred " + "and " +convert(int(input_int % 100))
```

⇩ converts a number to an English word with a specific condition of adding "," after thousands.
```
    if input_int==1000: 
        return  convert(input_int // 1000) + "thousand" + convert(int(input_int % 1000))
    if input_int==2000: 
        return  convert(input_int // 1000) + "thousand" + convert(int(input_int % 1000))
    if input_int==3000: 
        return  convert(input_int // 1000) + "thousand" + convert(int(input_int % 1000))
    if input_int==4000: 
        return  convert(input_int // 1000) + "thousand" + convert(int(input_int % 1000))
    if input_int==5000: 
        return  convert(input_int // 1000) + "thousand" + convert(int(input_int % 1000))
    if input_int==6000: 
        return  convert(input_int // 1000) + "thousand" + convert(int(input_int % 1000))
    if input_int==7000: 
        return  convert(input_int // 1000) + "thousand" + convert(int(input_int % 1000))
    if input_int==8000: 
        return  convert(input_int // 1000) + "thousand" + convert(int(input_int % 1000))
    if input_int==9000: 
        return  convert(input_int // 1000) + "thousand" + convert(int(input_int % 1000))
    elif input_int<1000000: 
        return  convert(input_int // 1000) + "thousand" + ", " + convert(int(input_int % 1000))
```


## Usage

You can run it eather on line or off line on a Terminal or Command Prompt. <br/>

>First you need go to direct path.<br/>
>Then type a command `python num_to_eng.py` and Enter.<br/>
>You will see a message "Input a number between 0-10000".<br/>
>And then you can type a number you want to convert.<br/>
>If the number is between 0-10000 it will be automatically converted to an English word through the functionality.<br/>
>If it is not in that range "Invalid number! Please input a number between 0-10000" message will be shown.<br/>

* for more detail please see thess attachments.

⇩ Running on a terminal in docker desktop app

![Running on terminal in docker desktop app](http://www.jk-learnkhmer.com/runnig-on-terminal-in-docker-desktop-app.jpg)

<br/>
⇩ Running on a terminal in localhost from docker desktop app

![Running on terminal in localhost from docker desktop app](http://www.jk-learnkhmer.com/runnig-on-terminal-in-localhost-from-docker-desktop-app.jpg)







